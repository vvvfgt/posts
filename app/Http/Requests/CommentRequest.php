<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        /* Different method have different rules */

        switch ($this->method()) {
            case 'POST' : {
                return [
                    'text' => 'required',
                    'post_id' => 'required_without:parent_id',  /* one of these fields must be present (post_id or parent_id) */
                    'parent_id' => 'required_without:post_id',
                ];
            }

            case 'PUT' : {
                return [
                    'text' => 'required'
                ];
            }
            default:
                break;
        }
    }
}
