<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $fillable =['post_id','text','parent_id'];

    /* each comment belong to a post */
    public function post()
    {
      return $this->belongsTo(Post::class);
    }

    /* each comment can have child comments */
    public function children()
    {
       return $this->hasMany(Comment::class,'parent_id');
    }

    /* create new comment*/
    public static function store($data)
    {
       /* Check input data */
       /* If isset field parent_id then it is children comment */
       if (isset($data['parent_id'])) {
          $comment_parent = Comment::find($data['parent_id']);
          $comment = self::create([
              'post_id' => $comment_parent->post_id,
              'text' => $data['text'],
              'parent_id' => $comment_parent->id
          ]);
       } else {
           /* it is comment for post*/
         //  $comment = self::create($data);
           $comment = self::create($data);

       }

       return $comment;

    }

}
