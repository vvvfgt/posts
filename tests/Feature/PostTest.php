<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PostTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    protected $new_post = 'Новый пост';
    protected $id = '1';
    protected $delete_id = '4';

    public function testIndex()
    {
        $response = $this->get('/api/posts');
        $response->assertOk();
    }

    /* not correct route 404 Not Found */
    public function testNoUrl()
    {
        $response = $this->get('/api/postss');
        $this->assertEquals(404,$response->status());
    }

    /* find post on id */
    public function testShow()
    {
        $response = $this->get('/api/posts/1');
        $response->assertOk();
    }

    /* if id is not found 404 Not Found */
    public function testShowNo()
    {
        $response = $this->get('/api/posts/125');
        $this->assertEquals(404,$response->status());
    }

    /* Create new post. must be a field title  */
    public function testCreate()
    {
        $response = $this->json('post','/api/posts',[
            'title' => $this->new_post,
        ]);
        $this->assertEquals('201',$response->status());

        /* get new id and check */
        $this->assertDatabaseHas('posts', [
            'title' => $this->new_post,
            'id' => $response->original['id']
        ]);
    }

    /* No found field title */
    public function testCreateNoTitle()
    {
        $response = $this->json('post','/api/posts');
        $response->assertStatus(422);
    }

    /* Must be field title */
    public function testUpdate()
    {
        $response = $this->put('/api/posts/'.$this->id,[
            'title' => $this->new_post,
        ]);
        $response->assertOk();
        $this->assertDatabaseHas('posts', [
           'title' => $this->new_post,
           'id' => $this->id
        ]);
    }

    public function testDelete()
    {
        $response = $this->delete('/api/posts/'.$this->delete_id);
        $this->assertDeleted('posts',[
            'id' => $this->delete_id
        ]);
    }

}
