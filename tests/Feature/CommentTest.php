<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommentTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    protected $new_comment = 'Новый комментарий';
    protected $post_id = 1;
    protected $parent_id = 1;
    protected $id = 1;
    protected $delete_id = 30;

    public function testIndex()
    {
        $response = $this->get('/api/comments');
        $this->assertEquals(200,$response->status());
    }

    /* not correct route 404 Not Found */
    public function testNoUrl()
    {
        $response = $this->get('/api/commentss');
        $this->assertEquals(404,$response->status());
    }

    /* find comment on id */
    public function testShow()
    {
        $response = $this->get('/api/comments/1');
        $this->assertEquals(200,$response->status());
    }


    /* Create new comment for post . must be a field text and post_id */
    public function testCreateForPost()
    {
        $response = $this->json('post','/api/comments',[
            'text' => $this->new_comment,
            'post_id' => $this->post_id
        ]);
        $this->assertEquals(201,$response->status());
        $this->assertDatabaseHas('comments', [
            'text' => $this->new_comment,
            'id' => $response->original['id'],
            'post_id' => $this->post_id,
            'parent_id' => null
        ]);
    }

    /* Create new children comment for comment . must be a field text and parent_id */
    public function testCreateForComment()
    {
        $response = $this->post('/api/comments',[
            'text' => $this->new_comment,
            'parent_id' => $this->parent_id
        ]);
        $this->assertEquals(201,$response->status());
        $this->assertDatabaseHas('comments', [
            'text' => $this->new_comment,
            'id' => $response->original['id'],
            'parent_id' => $this->parent_id
        ]);
    }

    /**
     * Create new children comment for comment . must be a field text.
     * If there are fields post_id and parent_id then field post_id will be ignore
     */

    public function testCreateForCommentAny()
    {
        $response = $this->post('/api/comments',[
            'text' => $this->new_comment,
            'parent_id' => $this->parent_id,
            'post_id' => $this->post_id
        ]);

        $this->assertEquals(201,$response->status());
        $this->assertDatabaseHas('comments', [
            'text' => $this->new_comment,
            'id' => $response->original['id'],
            'parent_id' => $this->parent_id
        ]);
    }

    /* No field text */

    public function testCreateNoText()
    {
        $response = $this->json('post','/api/comments',[
               'post_id' => $this->post_id,
               'parent_id' => $this->parent_id
            ]);

        $response->assertStatus(422);

    }

    /* Update text comments . Must be field text*/
    public function testUpdate()
    {
        $response = $this->put('/api/comments/'.$this->id,[
            'text' => $this->new_comment,
        ]);
        $response->assertOk();
        $this->assertDatabaseHas('comments', [
            'text' => $this->new_comment,
            'id' => $this->id
        ]);
    }


    public function testDelete()
    {
        $response = $this->delete('/api/comments/'.$this->delete_id);
        $this->assertDeleted('comments',[
            'id' => $this->delete_id
        ]);
        $this->assertDatabaseMissing('comments', [
            'id' => $this->delete_id
        ]);
    }


}
